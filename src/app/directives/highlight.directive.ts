import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef,
              private renderer: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.renderer.addClass(this.el.nativeElement, 'highlight');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.renderer.removeClass(this.el.nativeElement, 'highlight');

// @HostListener('mouseenter') onmouseenter(){
//   this.renderer.addClass(this.el.nativeElement, 'highlight');
//   // apply this highlight class to element when mouse moves into the region where elemnt is rendered in the view
// }

// @HostListener('mouseleave') onmouseleave(){
//   this.renderer.addClass(this.el.nativeElement, 'highlight');
//   // apply this highlight class to element when mouse moves into the region where elemnt is rendered in the view

}

}
