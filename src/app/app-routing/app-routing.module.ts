import { routes } from './routes';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes) //takes param
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
