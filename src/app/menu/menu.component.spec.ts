import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { from, Observable, of } from 'rxjs';
import { baseURL } from './../shared/baseurl';
import { DISHES } from './../shared/dishes';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DishService } from './../services/dish.service';
import { Dish } from './../shared/dish';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuComponent } from './menu.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

// need to explicitly import as not in touch with ngmodel as testing in isolation
describe('MenuComponent', () => {     // ''string specifies set of tetsts we are running, a description
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async () => {

    const dishServiceStub = {
      getDishes: function(): Observable<Dish[]>{
        return of(DISHES);
      }};
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FlexLayoutModule,
        RouterTestingModule.withRoutes([{path: 'menu', component: MenuComponent}]),
        MatGridListModule,
        MatProgressSpinnerModule
      ],  // test bed sets up environment for testing
      declarations: [ MenuComponent ],
      providers: [
        { provide: DishService, useValue: dishServiceStub },
        { provide: 'BaseURL', useValue: baseURL }
      ]
    })
    .compileComponents();

    const dishservice = TestBed.inject(DishService);
  });

  beforeEach(() => {   // whatever u declare here will be executed before each test
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {   // configure test here
    expect(component).toBeTruthy();  // this value should be true lol jasmine syntax
  });
  it('dishes items should be 4', () => {
    expect(component.dishes.length).toBe(4);
    expect(component.dishes[1].name).toBe('Zucchipakoda');
    expect(component.dishes[3].featured).toBeFalse();
  });

  it('should use dishes in template', () => {
    fixture.detectChanges();
    let de: DebugElement;
    let el: HTMLElement;
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;

    expect(el.textContent).toContain(DISHES[0].name.toUpperCase());
  });
});
