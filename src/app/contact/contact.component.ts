import { baseURL } from './../shared/baseurl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FeedbackService } from './../services/feedback.service';
import { Feedback, ContactType } from '../shared/feedback';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { flyInOut } from '../animations/app.animation';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})
export class ContactComponent implements OnInit {
  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackcopy: Feedback;
  errMess: string;
  contactType = ContactType;
  loading: boolean;
  loading2: boolean;
  demo: boolean;
  @ViewChild('fform') feedbackFormDirective;

 formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':      'Last Name is required.',
      'minlength':     'Last Name must be at least 2 characters long.',
      'maxlength':     'Last Name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':      'Tel. number is required.'
    },
    'email': {
      'required':      'Email is required.',
      'email':         'Email not in valid format.'
    },
  };

  constructor(private fb: FormBuilder,
              private feedbackservice: FeedbackService,
              private route: ActivatedRoute,
              private location: Location,
              @Inject('BaseURL') private BaseURL) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm(): void {
    this.loading = true;
    this.loading2 = true;
    this.demo = false;
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      telnum: ['', [Validators.required] ],
      email: ['', [Validators.required, Validators.email] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any): void {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit(): void {
    this.feedback = this.feedbackForm.value;
    this.feedbackcopy = this.feedback;
    console.log(this.feedback);
    this.loading = false;
    this.loading2 = false;
    this.feedbackservice.postFeedback(this.feedback)
    .subscribe(feedback => {
      this.loading = true;
      this.loading2 = false;
      this.demo = true;
      setTimeout( () => {this.demo = false; this.loading2 = true; }, 5000);
      this.feedback = feedback;
    },
    errmess => {this.feedback = null; this.errMess = <any>errmess; });
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackFormDirective.resetForm();

  }

}
