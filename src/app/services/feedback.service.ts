import { Feedback } from './../shared/feedback';
import { Injectable } from '@angular/core';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map, catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient,
              private ProcessHTTPMsgService: ProcessHTTPMsgService) { }

              postFeedback(feedback: Feedback): Observable<Feedback> {
                const httpOptions = {
                  headers: new HttpHeaders({
                    'Content-Type': 'applications/json'
                  })
                };
                return this.http.post<Feedback>(baseURL + 'feedback/', feedback)
                .pipe(catchError(this.ProcessHTTPMsgService.handleError), delay(3000));

              }
}
