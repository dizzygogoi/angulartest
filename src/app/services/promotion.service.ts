import { baseURL } from './../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PROMOTIONS } from '../shared/promotions';
import { Promotion } from '../shared/promotion';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: HttpClient,
              private ProcessHTTPMsgService: ProcessHTTPMsgService) { }


getPromotions(): Observable<Promotion[]> {
  return this.http.get<Promotion[]>(baseURL + 'promotions')
  .pipe(catchError(this.ProcessHTTPMsgService.handleError), delay(1000));
}

getPromotion(id: string): Observable<Promotion> {
  return this.http.get<Promotion>(baseURL + 'promotions' + id)
  .pipe(catchError(this.ProcessHTTPMsgService.handleError), delay(1000));
}

getFeaturedPromotion(): Observable<Promotion> {
  return this.http.get<Promotion[]>(baseURL + 'promotions?feeatured=true')
  .pipe(map(promotion => promotion[0]))
  .pipe(catchError(this.ProcessHTTPMsgService.handleError), delay(1000));
}
}
